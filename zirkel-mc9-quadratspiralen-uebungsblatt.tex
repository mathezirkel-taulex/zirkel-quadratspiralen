% Hier den Pfad zu zirkel.cls angeben
\documentclass{../tex-bilder/zirkel}

\graphicspath{{../tex-bilder/}}

\usepackage{mdframed}
\usepackage{array}      % for spacing in tabular

\usepackage{wrapfig,lipsum}     % image to the side with text wrapped around

\renewcommand{\schuljahr}{2022/23}
\renewcommand{\klasse}{9}
\renewcommand{\titel}{Vertiefung: Unendlicher-Abstieg-Quadratspiralen}
\renewcommand{\untertitel}{Aus Mathologers "The golden ratio spiral: visual infinite descent", Mathecamp 2023}
\renewcommand{\name}{Alexander Mai}

% Vordefinierte Umgebungen
%     * aufgabe
%     * hinweis
%     * loesung
%     * satz
%     * beispiel

\begin{document}
    \makeheader
    \vspace*{-1em}
    
    Die folgenden Aufgaben können unabhängig voneinander in beliebiger Reihenfolge bearbeitet werden. Die Teilaufgaben innerhalb einer Aufgabennummer bauen teilweise aufeinander auf.

    \begin{figure}[h!]
        \centering
        \includegraphics[width=\linewidth]{quadratspiralen/sqrt-examples.pdf}
    \end{figure}
    \vspace*{-1em}


    \section{Wiederholung}
    \vspace*{-1.5em}

    \begin{aufgabe}
        \vspace*{-1em}
        \begin{enumerate}[a)]
            \item Wiederhole den Beweis aus dem Zirkel, bei dem wir mit Hilfe einer Quadratspirale bewiesen haben, dass $\sqrt{3}$ irrational ist.
            \item Beweise, dass eine unendliche Quadratspirale bedeutet, dass das Verhältnis des Startrechtecks eine irrationale Zahl ist. \textbf{Tipp:} Beweise die äquivalente Kontraposition: Bei einem rationalen Verhältnis ist die Quadratspirale endlich.
            \item Beweise die Rückrichtung: bei einer endlichen Quadratspirale ist das Verhältnis des Startrechtecks eine rationale Zahl. \textbf{Hinweis:} Die Quadratspirale ist zwar endlich, aber du kannst dadurch nicht einfach annehmen, dass das kleinste Quadrat eine rationale Seitenlänge hat.
            \item Wie erhält man aus der Quadratspirale die Kettenbruchentwicklung einer Zahl?
        \end{enumerate}
    \end{aufgabe}


    \vspace*{1em}
    \section{Den Perioden auf der Spur}
    \vspace*{-1.5em}

    \begin{aufgabe}
        Untersuche mithilfe der Quadratspiralen die Irrationalität von:

        \vspace*{0.5em}
        \hspace*{1em}
            a) $\sqrt{2}$ \hfill
            b) $\sqrt{5}$ \hfill
            c) $\sqrt{9}$
        \hspace*{11em}
        \vspace*{0.8em}
        
        \textbf{Tipp:} zeichne zuerst die Quadratspirale, damit du eine Vermutung hast, wann sich das ursprüngliche Verhältnis wieder zeigt, was du dann beweisen kannst. Damit du die Zeichnung anfertigen kannst, kannst du natürlich einen Taschenrechner zur Berechnung der Wurzeln hernehmen, anstatt sie zu schätzen.
    \end{aufgabe}


    \newpage

    \vspace*{-6em}

    \section{Der euklidische Algorithmus}
    \vspace*{-0.5em}

    In Aufgabe~\ref{ex:euclidandspirals} brauchst du für die erste Teilaufgabe den euklidischen Algorithmus nicht. Lese ihn dir gerne erst für die Teilaufgaben nach der ersten durch.

    \vspace*{-0.5em}
    \subsection*{Größter gemeinsamer Teiler -- ggT}
    \vspace*{-0.5em}

    Mit dem \emph{euklidischen Algorithmus} lässt sich der größte gemeinsame Teiler -- geschrieben $\operatorname{ggT}(a, b)$ -- zweier natürlicher Zahlen $a, b \in \mathbb{N}$ berechnen. Es ist die größte Zahl, durch die beide Zahlen teilbar sind, sodass sie danach beide noch ganze Zahlen sind, also: $\dfrac{a}{\operatorname{ggT}(a, b)} , \dfrac{b}{\operatorname{ggT}(a, b)} \in \mathbb{N} .$

    \vspace*{-0.5em}
    \subsection*{Euklidischer Algorithmus}
    \vspace*{-0.5em}

    Für den Algorithmus müssen wir bei jedem Schritt einfach eine größere Zahl teilen durch eine kleinere Zahl und den Rest bestimmen, der dabei übrigbleibt. Für den nächsten Schritt teilen wir die kleinere Zahl durch den Rest -- und das wiederholen wir dann solange, bis der Rest einer Rechnung $0$ ist. Der zuletzt benutzte Teiler ist dann der größte gemeinsame Teiler. Knapper gefasst, schreiben wir den Algorithmus folgendermaßen auf. Dafür nehmen wir an, dass $a$ die größere Zahl ist. Die kleinere Zahl $b$ schreiben wir im ersten Schritt als $r_0$ :

    \vspace*{-2em}
    \begin{align*}
        a          &= q_1 \cdot r_0 + r_1 \\
        r_0        &= q_2 \cdot r_1 + r_2 \\
        r_1        &= q_3 \cdot r_2 + r_3 \\
                   &\vdots \\
        r_{n-1}    &= q_{n+1} \cdot r_n + 0
    \end{align*}
    \vspace*{-1.5em}

    Der Teiler $r_n$ der letzten Rechnung ist dann der ggT. Hier ein Beispiel, $\operatorname{ggT}({\color{OliveGreen}\textbf{1071}}, {\color{RedOrange}\textbf{462}}) = {\color{RubineRed}\textbf{21}}$:

    \vspace*{-0.5em}
    $$\begin{array}{rclll}
        {\color{OliveGreen}\textbf{1071}}  & \textbf{=} & \textbf{2} \cdot {\color{RedOrange}\textbf{462}} & \textbf{+} &{\color{RoyalBlue}\textbf{147}} \\
        {\color{RedOrange}\textbf{462}}  & \textbf{=} & \textbf{3} \cdot {\color{RoyalBlue}\textbf{147}} & \textbf{+} &{\color{RubineRed}\textbf{21}} \\
        {\color{RoyalBlue}\textbf{147}} & \textbf{=} & \textbf{7} \cdot {\color{RubineRed}\textbf{21}}    & \textbf{+} &\textbf{0}
    \end{array}$$

    % \begin{align*}
    %     1071    & = 2 \cdot 462     & + 147 \\
    %     462     & = 3 \cdot 147     & + 21 \\
    %     147     & = 7 \cdot 21      & + 0
    % \end{align*}

    Wenn du willst, kannst bei der letzten Aufgabe weitere Beispiele durchrechnen.

    \begin{aufgabe}[ggT und Quadratspiralen]\label{ex:euclidandspirals}
        Für diese Aufgabe betrachten wir nur endliche Quadratspiralen. Es gibt also auch immer ein kleinstes Quadrat.

        \vspace*{-0.5em}
        \begin{enumerate}[a)]
            \item Beweise, dass das kleinste Quadrat alle anderen Quadrate teilt, also perfekt alle anderen ausfüllen kann.
            \vspace*{-0.5em}
            \item Finde den Zusammenhang zwischen euklidischem Algorithmus und Quadratspiralen.
            \vspace*{-0.5em}
            \item Seien die Seitenlängen unseres Startrechtecks $A, B \in \mathbb{N} .$ Beweise, dass das kleinste Quadrat die Seitenlänge $\operatorname{ggT}(A, B)$ hat. \textbf{Tipp:} Überlege dir, was passiert, wenn du die Seitenlängen des Startrechtecks "kürzt".
            \vspace*{-0.5em}
            \item \textbf{Bonus:} Welche Konsequenz hat $\operatorname{ggt}(A, B) = 1$ für die Primfaktorzerlegung von $A$ und $B$?
        \end{enumerate}
    \end{aufgabe}
    \vspace*{-1em}



    \enlargethispage{3\baselineskip}

    \begin{aufgabe}[Ein paar Beispiele]
        Hier sind ein paar Beispielaufgaben zum euklidischen Algorithmus. Rechne sie nur, wenn du dich mit der oberen Beschreibung des euklidischen Algorithmus schwertust oder den Algorithmus ein bisschen üben willst. Berechne den ggT mit Hilfe des euklidischen Algorithmus:

        \vspace*{0.5em}
        \hspace*{1em}
            a) $133, 98$ \hfill
            b) $83, 79$ \hfill
            c) $925, 65$ \hfill
            d) $1247, 989$ \hfill
            e) $1319, 953$
        \hspace*{6em}

        % \begin{enumerate}[a)]
        %     \item $\operatorname{ggT}(133, 98)$ \hfill
        %     \item $\operatorname{ggT}(83, 79)$ \hfill
        %     \item $\operatorname{ggT}(925, 66)$ \hfill
        %     \item $\operatorname{ggT}(1247, 989)$ \hfill
        %     \item $\operatorname{ggT}(1319, 953)$
        % \end{enumerate}
    \end{aufgabe}

\end{document}